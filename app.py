import imp
from multiprocessing.spawn import import_main_path
from flask import Flask, render_template

app = Flask(__name__)

@app.route("/")
def main() -> str:
    return render_template("index.html")

@app.route("/inputbarang")
def inputbarang() -> str:
    return render_template("mod_form/inputbarang.html", menu="barangmasuk", submenu="forminputbarang")

@app.route("/databarang")
def databarang() -> str:
    return render_template('databarang.html', menu="databarang", submenu="listdatabarang")

if __name__ == "__main__":
    app.run(debug=False)